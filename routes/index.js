exports.index = function(req, res, next) {
    res.render('default', {
        title: 'Home',
        classname: 'home',
        users: ['Shams', 'Nahid']
    });
}

exports.about = function(req, res, next) {
    res.render('default', {
        title: 'About Us',
        classname: 'about'
    });
}