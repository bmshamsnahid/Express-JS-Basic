var express = require('express');
var app = express();
var routes = require('./routes');

app.set('view engine', 'ejs');

app.locals.pagetitle = 'Awsome Site';

app.get('/', routes.index);
app.get('/about', routes.about);

app.get('*', function (req, res, next) {
    res.send('bad routing');
});

var server = app.listen(3000, function () {
    console.log('Listening on port 3000');
});